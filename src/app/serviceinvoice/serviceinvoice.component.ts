import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-serviceinvoice',
  templateUrl: './serviceinvoice.component.html',
  styleUrls: ['./serviceinvoice.component.less']
})
export class ServiceinvoiceComponent implements OnInit {
  customerId: any;
  customertype: any;
  name: any;
  mailid: any;
  address: any;
  mobileno: any;
  model: any;
  type: any;
  sparepart: any;
  productname: any;
  dateofpurchase: any;
  dateofservicerequest: any;
  iswarranty: any;
  charge: any;
  techniciancost: any;
  tax: any;
  totalcost: any;
  paidamount: any;
  ticketno: any;
  dateofresolvingissue: any;
  servicingdate: any;
  remainingamount: any;
  ticketstatus: any;
  invoiceno: any;
  data: any = []
  constructor() { }

  ngOnInit() {
   var datas =  localStorage.getItem('categories')
   this.data.push(JSON.parse(datas))
   this.data = this.data[0]
   this.customerId = this.data.customerids
  
   this.customertype = this.data.datacustomertype
   this.name = this.data.name
   console.log(this.name)
   this.mailid = this.data.mailid
   this.address = this.data.address
   this.mobileno = this.data.mobileno
   this.model =  this.data.model
   this.type =  this.data.type
   this.sparepart =  this.data.sparepart
   this.productname =  this.data.productname
   console.log(this.productname)
   this.dateofpurchase =  this.data.dateofpurchase
   this.dateofservicerequest =  this.data.dateofservicerequest
   this.iswarranty =  this.data.iswarranty
   this.charge =  this.data.charge
   this.techniciancost =  this.data.techniciancost
   this.tax =  this.data.tax
   this.totalcost =  this.data.totalcost
   this.paidamount =  this.data.paidamount
   this.ticketno =  this.data.ticketno
   this.dateofresolvingissue =  this.data.dateofresolvingissue
   this.servicingdate =  this.data.servicingdate
   this.remainingamount =  this.data.remainingamount
   this.ticketstatus =  this.data.ticketstatus
   this.ticketno=this.data.ticketno
   this.invoiceno =  this.data.invoiceno
  }
  print(event){
    window.print()
  }
}
