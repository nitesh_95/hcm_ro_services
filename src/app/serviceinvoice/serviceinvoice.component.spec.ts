import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceinvoiceComponent } from './serviceinvoice.component';

describe('ServiceinvoiceComponent', () => {
  let component: ServiceinvoiceComponent;
  let fixture: ComponentFixture<ServiceinvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceinvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceinvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
