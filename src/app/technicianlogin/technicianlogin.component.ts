import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-technicianlogin',
  templateUrl: './technicianlogin.component.html',
  styleUrls: ['./technicianlogin.component.less']
})
export class TechnicianloginComponent {


  username: any
  password: any;
  invalidLogin = false

  common_IP: any;
  constructor(private router: Router,
    private http: HttpClient) { }
  //    ROOT_URL_DEV: string = 'http://localhost:5000'
  // ROOT_URL_ANGULAR:string='http://localhost:4200'
  ROOT_URL_DEV: string ='http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'
  ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'
    checkLogin(event) {
      var selected_id = event.currentTarget.id
      const base_URL = this.ROOT_URL_DEV+'/api/excel/technicianLogin/' + this.username + '/' + this.password
      this.http.post(base_URL, {
      }).subscribe(data => {
        if (data['status'] == '200') {
          localStorage.setItem('servicingEntities', JSON.stringify(data['servicingEntities']))
          localStorage.setItem('loginname',JSON.stringify(data['name']))
          alert('Welcome :- ' + data['name'])
          // window.open('http://localhost:4200/techniciandashboard')
          this.router.navigate(['techniciandashboard'])
        } else {
          alert('Something went Wrong')
          window.location.reload()
        }
      })
    }
  }