import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-addtask',
  templateUrl: './addtask.component.html',
  styleUrls: ['./addtask.component.less']
})
export class AddtaskComponent implements OnInit {

  constructor(private router: Router){
  }

  ngOnInit() {
  }
  
  logOut(event){
    sessionStorage.clear()
    this.router.navigate(['login']);
  
   }
}
