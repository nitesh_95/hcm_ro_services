import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicianportalComponent } from './technicianportal.component';

describe('TechnicianportalComponent', () => {
  let component: TechnicianportalComponent;
  let fixture: ComponentFixture<TechnicianportalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicianportalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicianportalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
