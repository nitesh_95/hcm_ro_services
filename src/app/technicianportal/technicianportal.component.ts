import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-technicianportal',
  templateUrl: './technicianportal.component.html',
  styleUrls: ['./technicianportal.component.less']
})
export class TechnicianportalComponent implements OnInit {

  ticketno:string
  mobileno:any
  constructor(private router: Router,
    private http: HttpClient) { }

    // ROOT_URL_DEV: string = 'http://localhost:5000'
  //ROOT_URL_ANGULAR:string='http://localhost:4200'
  ROOT_URL_DEV: string ='http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'
  ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'
  ngOnInit() {
  }
  checkLogins(event){
    this.router.navigate(['techniciantble'])
  }
  checkLogin(event) {
    var selected_id = event.currentTarget.id
    const base_URL = this.ROOT_URL_DEV+'/api/excel/getbyticketnoandmobileno/'+this.ticketno+'/'+this.mobileno
    this.http.get(base_URL, {
    }).subscribe(data => {
      if (data['status'] == '200' ) {
        localStorage.setItem('servicingdetails',JSON.stringify(data['servicingEntity']))
        this.router.navigate(['techniciandetails'])
      } else {
        alert('Check Your Invoice Details and Retry Again')
       window.location.reload()
      }
    })
  }
}
