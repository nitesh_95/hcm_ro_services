import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceapplyComponent } from './serviceapply.component';

describe('ServiceapplyComponent', () => {
  let component: ServiceapplyComponent;
  let fixture: ComponentFixture<ServiceapplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceapplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceapplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
