import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent  implements OnInit{

  employeedata:any
  todocountData:any
  companyData:any

  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {
    this.getemployeeData(event)
    this.getToDoCount(event)
    this.getCompanyData(event)
   
  }

  logOut(event){
    sessionStorage.clear()
    alert("Are you sure you want to Exit ?")
    this.router.navigate(['login']);
  
}

  
  public lineChartData: ChartDataSets[] = [
    { data: [61, 59, 80, 65, 45, 55, 40, 56, 76, 65, 77, 60], label: 'Sales' },
    { data: [57, 50, 75, 87, 43, 46, 37, 48, 67, 56, 70, 50], label: 'Profit' },
  ];
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public lineChartOptions: (ChartOptions & {  }) = {
    responsive: true,
  };
   
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];
  
  getemployeeData(event){
    const base_url = "http://localhost:8990/count"
    this.http.post(base_url,{
  
    }).subscribe(data => {
      
     this.employeedata = data;
     console.log(this.employeedata)
    })
  }
  getCompanyData(event){
    const base_url = "http://localhost:8992/count"
    this.http.post(base_url,{
  
    }).subscribe(data => {
      console.log(data)
      this.companyData= data
    })
  }
  getToDoCount(event){
    const base_url = "http://localhost:8991/dataCount"
    this.http.post(base_url,{
  
    }).subscribe(data => {
      console.log(data)
      this.todocountData = data
    })
  }

}
