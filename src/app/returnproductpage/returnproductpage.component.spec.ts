import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnproductpageComponent } from './returnproductpage.component';

describe('ReturnproductpageComponent', () => {
  let component: ReturnproductpageComponent;
  let fixture: ComponentFixture<ReturnproductpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReturnproductpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnproductpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
