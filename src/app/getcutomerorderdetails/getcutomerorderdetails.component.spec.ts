import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetcutomerorderdetailsComponent } from './getcutomerorderdetails.component';

describe('GetcutomerorderdetailsComponent', () => {
  let component: GetcutomerorderdetailsComponent;
  let fixture: ComponentFixture<GetcutomerorderdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetcutomerorderdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetcutomerorderdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
