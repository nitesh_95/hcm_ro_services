import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-offlinecustomerdetailsdata',
  templateUrl: './offlinecustomerdetailsdata.component.html',
  styleUrls: ['./offlinecustomerdetailsdata.component.less']
})
export class OfflinecustomerdetailsdataComponent implements OnInit {

  amount: any = []
  offlinecustomerid: any;
  invoiceno: any;
  offlinecustomerdetails: any;
  fullamount: any;
  gstrate: any;
  otherTaxRate: any;
  entirerate: any;
  othercharges: any;
  paidamount: any;
  remainingamount: any;
  emiapplicable: boolean;
  dateofpurchase: any;
  noofmonths: any;
  montlyinstallment: any;
  retrievedObject: any
  name: any
  mailid: any
  tableview: boolean
  mobileno: any
  myarray: any = []
  arr: any = []
  myarrays: any = []
  emiData: boolean
  paymentpending: boolean
  fullpayment: boolean
  id: any;
  emailid: any;
  address: any;
  postalcode: any;
  is_cancelled: any;
  purchasedate: any;
  modeofpayment: any;
  totalamount: any;
  purchaseid: any;
  paymentid: any;
  paymentstatusss: any;
  dateofdeliverys: any;
  orderstatusss: any;
  paymentdates: any;
  transactionids: any;
  warrantystatus: any;
  paymentamount: Number
  paymentdate: any
  pageSize=5
  PromoCode: any;
  promocodes: any;

  constructor(private http: HttpClient) { }
 // ROOT_URL_DEV: string = 'http://localhost:5000'
  //ROOT_URL_ANGULAR:string='http://localhost:4200'
  ROOT_URL_DEV: string ='http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'

  ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'
  ngOnInit() {
    this.offlinecustomerdetails = JSON.parse(localStorage.getItem('offlinecustomerdata'))
    console.log(this.offlinecustomerdetails)
    this.offlinecustomerid = this.offlinecustomerdetails.offlinecustomerid
    this.dateofpurchase = this.offlinecustomerdetails.dateofpurchase
    this.invoiceno = this.offlinecustomerdetails.invoiceno
    this.name = this.offlinecustomerdetails.name
    this.mailid = this.offlinecustomerdetails.mailid
    this.mobileno = this.offlinecustomerdetails.mobileno
    this.fullamount = this.offlinecustomerdetails.billingdetails.fullamount
    this.gstrate = this.offlinecustomerdetails.billingdetails.gstrate
    this.PromoCode= this.offlinecustomerdetails.billingdetails.promocodeapplieds
    this.promocodes = this.offlinecustomerdetails.billingdetails.promocodevalues
    this.otherTaxRate = this.offlinecustomerdetails.billingdetails.otherTaxRate
    this.entirerate = this.offlinecustomerdetails.billingdetails.entirerate
    this.othercharges = this.offlinecustomerdetails.billingdetails.othercharges
    this.paidamount = this.offlinecustomerdetails.billingdetails.paidamount
    this.emiapplicable = this.offlinecustomerdetails.billingdetails.emiapplicable
    if (this.noofmonths!= null || this.montlyinstallment != null || this.offlinecustomerdetails.billingdetails.emidata!=null) {
     

      this.noofmonths = this.offlinecustomerdetails.billingdetails.emidata.noofmonths
      this.montlyinstallment = this.offlinecustomerdetails.billingdetails.emidata.montlyinstallment
      this.myarrays.push(this.offlinecustomerdetails.billingdetails.emidata.emiDatas)
      this.myarrays = this.myarrays[0]
       
    } else {
      this.montlyinstallment = 0
      this.myarrays = null
      this.noofmonths = 0
    }

    this.remainingamount = this.offlinecustomerdetails.billingdetails.remainingamount
    if (this.offlinecustomerdetails.billingdetails.emiapplicable) {
      this.emiapplicable = true
      this.tableview = true
    } else {
      this.tableview = false
      this.emiapplicable = false
    }

    console.log("monthly installment is" + this.montlyinstallment)
    this.myarray.push(this.offlinecustomerdetails.productPurchaseds)
    this.myarray = this.myarray[0]
    this.arr.push(this.offlinecustomerdetails.returnproductname)
    this.arr = this.arr[0]
    console.log('THe array is'+ this.arr)
  }
  updateRequest(event) {
    this.myarrays.push(
      { paidamount: this.paymentamount, date: this.paymentdate }
    );
    const base_URL = this.ROOT_URL_DEV+'/api/excel/saveofflinecustomerdetails'
    this.http.post(base_URL, {
      offlinecustomerid: this.offlinecustomerid,
      name: this.name,
      mailid: this.mailid,
      mobileno: this.mobileno,
      invoiceno: this.invoiceno,
      dateofpurchase: this.dateofpurchase,
      productPurchaseds: this.myarray,
      billingdetails: {
        gstrate: this.gstrate,
        otherTaxRate: this.otherTaxRate,
        othercharges: this.othercharges,
        entirerate: this.entirerate,
        paidamount: this.paidamount,
        emiapplicable: this.emiapplicable,
        promocodeapplieds:this.PromoCode,
        promocodevalues:this.promocodes,
        emidata: {
          noofmonths: this.noofmonths,
          montlyinstallment: this.montlyinstallment,
          emiDatas: this.myarrays
        }
      }
    }).subscribe(data => {
      console.log(this.myarrays)
      alert('Order Details has been sucessfully Updated ')
      window.location.href = this.ROOT_URL_ANGULAR+'/offlinecustomerdetails'
    })
  }
}

function appendObjTo(myArray: any, arg1: { hello: string; }) {
  throw new Error('Function not implemented.');
}
