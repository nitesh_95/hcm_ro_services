import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import 'bootstrap';


@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.less']
})
export class AddProductComponent implements OnInit {
  id: string;
  router: any;
  http: any;
  compname: any;
  allocated: any;
  category: any;
  employeedetailsList: any;
  pricing: any
  Stock: any
  OnlineStore: any

  constructor() { }

  local

   // ROOT_URL_DEV: string = 'http://localhost:5000'
   ROOT_URL_DEV: string ='http://delightro-env.eba-6vgctuqr.ap-south-1.elasticbeanstalk.com'
   ROOT_URL_ANGULAR:string='http://delightropoints.s3-website.ap-south-1.amazonaws.com/'

  sessionStorage

  ngOnInit() {
    this.id = sessionStorage.getItem('username')
  }

  
  logOut(event) {
    sessionStorage.clear()
    this.router.navigate(['login']);

  }
  clickSubmit(event) {
    const base_URL = this.ROOT_URL_DEV + '/save'
    this.http.post(base_URL, {
      compname: this.compname,
      allocated: this.allocated,
      category: this.category,
    }).subscribe(data => {
      console.log(data['status'])
      if (data['status'] == '00') {
        alert("Employee has been Added Successfully")
        window.location.reload();
      }

    })
  }


  getAllCompName(event) {
    const base_URL = this.ROOT_URL_DEV + '/getAll'
    this.http.post(base_URL, {
    }).subscribe(data => {

      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]
      console.log(data)

    })
  }
  submitModals(event) {
  }
  delete(event) {
    var selected_id = event.currentTarget.id
    alert("Are you sure you want to delete the Data??")
    const base_URL = this.ROOT_URL_DEV + '/delete/' + selected_id
    this.http.post(base_URL, {}).subscribe(data => {
      console.log(data)
      if (data['status'] == '00') {
        alert("Data has been sucessfully deleted")
        window.location.reload();
      }
    })
  }
}