import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockproducttableComponent } from './stockproducttable.component';

describe('StockproducttableComponent', () => {
  let component: StockproducttableComponent;
  let fixture: ComponentFixture<StockproducttableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockproducttableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockproducttableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
