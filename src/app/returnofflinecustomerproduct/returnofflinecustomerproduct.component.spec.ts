import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnofflinecustomerproductComponent } from './returnofflinecustomerproduct.component';

describe('ReturnofflinecustomerproductComponent', () => {
  let component: ReturnofflinecustomerproductComponent;
  let fixture: ComponentFixture<ReturnofflinecustomerproductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReturnofflinecustomerproductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnofflinecustomerproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
