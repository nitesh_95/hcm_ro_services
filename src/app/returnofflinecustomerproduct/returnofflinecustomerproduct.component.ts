import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-returnofflinecustomerproduct',
  templateUrl: './returnofflinecustomerproduct.component.html',
  styleUrls: ['./returnofflinecustomerproduct.component.less']
})
export class ReturnofflinecustomerproductComponent implements OnInit {
  customerids: any;
  name: any;
  mailid: any;
  arrayz: any = []
  address: any;
  mobileno: any;
  alternatemobileno: any;
  model: any;
  type: any;
  charge: any;
  issuedescription: any
  paymentdate: any;
  sparepart: any;
  date: any;
  arrays: any = []
  categories: any = []
  techniciancost: any;
  data: any;
  isButtonEnabled: boolean = true
  employeedetailsLists: any = [];
  array: any = [];
  productnames: any;
  unitprice: any;
  isButtonEnableds: boolean
  existingcustomer: any
  offlinecustomer: boolean
  onlinecustomer: boolean
  offlinecustomerdata: boolean
  options: string[] = ["OfflineCustomer"];
  optionss: string[] = ["inWarranty", "outOfWarranty"];
  selectedQuantity = "10";

  datas: any
  invoiceno: string;
  isButtonEnableddata: boolean;
  tax: any;
  totalcost: any;
  enabledbutton: boolean = true
  paidamount: any;
  names: any;
  mobilenos: any;
  arraysjs: any = []
  mailids: any;
  dateofpurchases: any;
  addresss: any;
  invoicenos: any;
  arr: any = [];
  productpurchased: boolean
  productpurchaseds: boolean
  newcustomer: boolean
  productnamesdatas: any;
  datass: any;
  productnamesdatasvalue: any
  dateofservicerequest: any;
  servicingdate: any;
  isButtonEnabledss: boolean
  enabledbuttons: boolean
  totalcostx: Object;
  employeeNames: any;
  totalcostAfterapplyingdeliverycharges: any;
  dateofdelivery: any;
  orderstatus: any;
  paymentstatus: any;
  refundmoney: Object;
  deliverycharges: any;
  id: any;
  paymentid: any;
  refundedamount: any;
  myarray: any =[];
  customerdetails: any;
  entirerate: any;
  offlinecustomerid: any;
  constructor(private router: Router, private http: HttpClient) { }
  // ROOT_URL_DEV: string = 'http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'
  // ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'
  ngOnInit() {
    this.getemployeedetails(event)
  }
  ROOT_URL_DEV: string = 'http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'
  ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'

  logOut(event) {
    sessionStorage.clear()
    alert("Are you sure you want to Exit ?")
    this.router.navigate(['login']);

  }

  onOptionsSelectedsdata(selectedValue) {
    this.datas = selectedValue
    console.log(this.datas)
    if (this.datas == 'OfflineCustomer' ) {
      this.enabledbutton = false
      this.offlinecustomer = true
      this.offlinecustomerdata = false
      this.enabledbuttons = true
      this.isButtonEnabledss = false
      const base_URL = this.ROOT_URL_DEV + '/api/excel/servicingdetailslist'
      this.http.get(base_URL, {
      }).subscribe(data => {
        console.log(data)
      })
    } else if (this.datas == 'OnlineCustomer') {
      this.enabledbutton = false
      this.offlinecustomer = false
      this.offlinecustomerdata = true
      this.isButtonEnabled = false
      this.isButtonEnabledss = true
    }
  }

  onOptionsSelecteds(selectedValue) {
    this.productnames = selectedValue
    this.unitprice = selectedValue.unitprice
    console.log(this.unitprice)
  }

  


  
  submitsdata(event) {
    this.submitdata(event)
    window.location.href = this.ROOT_URL_ANGULAR + '/returnpage'
  }
  submit(event) {
    if (!this.invoiceno || !this.mobileno) {
      alert('Every Field Is Mandatory')
      this.isButtonEnableds = false
      this.isButtonEnableddata = false
    } else if (this.datas == 'OnlineCustomer') {
      alert('Please Click one more time to confirm the details')
      this.isButtonEnableddata = true
      this.isButtonEnabled = false
      this.isButtonEnableds = false
      this.newcustomer = true
      this.enabledbuttons = true
      this.isButtonEnabledss = false
    } else if (this.datas == 'OfflineCustomer') {
      alert('Please Click one more time to confirm the details')
      this.enabledbuttons = true
      this.isButtonEnabledss = false
      this.isButtonEnableddata = false
      this.isButtonEnabled = false
      this.isButtonEnableds = true
      this.newcustomer = true
    } else {
      this.isButtonEnabledss = true
    }
  }

  warrantyStatus(selectedQuantity) {
    if (selectedQuantity == 'inWarranty') {
      this.datass = true
    } else {
      this.datass = false
    }
  }
  onOptionsSelectedsdatas(selectedValue) {
    if (this.datas == 'OnlineCustomer')
      if (selectedValue != undefined) {
        this.productnamesdatas = selectedValue.productname
        console.log(this.productnamesdatas)
      } else {
        this.productnamesdatas = selectedValue.productnames.productname
        console.log(selectedValue.productname)
      }
  }
  onOptionsSelectedsdat(selectedValue) {
    this.employeeNames = selectedValue
    console.log(this.employeeNames)
  }
  getemployeedetails(event) {
    const base_URL = this.ROOT_URL_DEV + '/api/excel/getDetailsdata'
    this.http.get(base_URL,
    ).subscribe(data => {
      this.arrays.push(data)
      this.arrays = this.arrays[0]
      console.log('data is' + data)
      console.log(this.arrays)
    })
  }
  getofflinedata() {
    const base_URL = this.ROOT_URL_DEV + '/api/excel/offlinecustomerbyid/' + this.invoiceno + '/' + this.mobileno
    this.http.get(base_URL,
    ).subscribe(data => {
      localStorage.setItem('offlinedetailsvalue', JSON.stringify(data))
      this.offlinecustomerid =data['offlineCustomerDetails'].offlinecustomerid 
      this.names = data['offlineCustomerDetails'].name
      this.mobilenos = data['offlineCustomerDetails'].mobileno
      this.mailids = data['offlineCustomerDetails'].mailid
      this.dateofpurchases = data['offlineCustomerDetails'].dateofpurchase
      this.addresss = data['offlineCustomerDetails'].address
      this.invoicenos = data['offlineCustomerDetails'].invoiceno
      this.entirerate = data['offlineCustomerDetails'].billingdetails.entirerate
      if(data['offlineCustomerDetails'].billingdetails.emidata==null){
        this.paymentstatus = 'paid'
      }else{
        this.paymentstatus = 'onEMI'
      }
      console.log('data  in list'+data['offlineCustomerDetails'].productPurchaseds)
      this.arr.push(data['offlineCustomerDetails'].productPurchaseds)
      this.arr = this.arr[0]
      console.log('data in the list is '+this.arr)
    })

  }

  submits(event) {
    const base_URL = this.ROOT_URL_DEV + '/api/excel/offlinecustomerbyid/' + this.invoiceno + '/' + this.mobileno
    this.http.get(base_URL,
    ).subscribe(data => {
      console.log(data)
      if (data['status'] == '200') {
        this.offlinecustomerdata = true
        this.productpurchaseds = true
        this.productpurchased = false
        alert('Service Data Updated')
        this.getofflinedata()
        this.enabledbuttons = true
        this.isButtonEnabledss = true
        this.isButtonEnableds = false
      } else {
        alert('No Such Customer Found')
        window.location.reload()
      }
    })
  }
  submitdatas(event) {
    const base_URL = this.ROOT_URL_DEV + '/api/excel/onlinecustomerid/' + this.invoiceno + '/' + this.mobileno
    this.http.get(base_URL,
    ).subscribe(data => {
      console.log(data)
      if (data['status'] == '200') {
        this.offlinecustomerdata = true
        this.productpurchased = true
        this.productpurchaseds = false
        alert('Service Data Updated')
        this.isButtonEnableddata = false
        this.enabledbuttons = true
        this.isButtonEnabledss = true
        this.isButtonEnableds = false
      } else {
        alert('No Such Customer Found')
        window.location.reload()
      }
    })
  }

  submitss(event) {
    const base_URL = this.ROOT_URL_DEV + '/api/excel/onlinecustomerid/' + this.invoiceno + '/' + this.mobileno
    this.http.get(base_URL,
    ).subscribe(data => {
      console.log(data)
      if (data['status'] == '200') {
        alert('Service Data Updated')
        window.location.href = this.ROOT_URL_ANGULAR + '/customer'
      } else {
        alert('No Such Customer Found')
        window.location.reload()
      }
    })
  }

  gettotalcost(event) {
    const base_URL = this.ROOT_URL_DEV + '/api/excel/gettotalamount'
    this.http.post(base_URL, {
      charge: this.unitprice,
      techniciancost: this.techniciancost,
      tax: this.tax,
    }).subscribe(data => {
      console.log(data)
      this.totalcostx = data
    })
  }
  onOptionsSelectedsdatass(selectedValues) {
    this.unitprice = selectedValues.unitprice
    this.productnamesdatasvalue = selectedValues.addProductInStock.itemname
    console.log(selectedValues)
  }
  submitdata(event) {
    console.log(this.name, this.mailid, this.address, this.mobileno, this.unitprice, this.paidamount, this.model, this.invoiceno, this.productnamesdatasvalue,
      this.productnamesdatas, this.dateofpurchases, this.dateofservicerequest, this.tax, this.servicingdate, this.techniciancost)
    if (!this.name) {
      alert('Name is not properly defined')
    } else if (!this.mailid) {
      alert('MailId is not properly defined')
    } else if (!this.address) {
      alert('address is not properly defined')
    }
    else if (!this.mobileno) {
      alert('mobileno is not properly defined')
    } else {
      const base_URL = this.ROOT_URL_DEV + '/api/excel/servicingdetails'
      this.http.post(base_URL, {
        customertype: this.datas,
        name: this.name,
        mailid: this.mailid,
        address: this.address,
        mobileno: this.mobileno,
        model: this.model,
        type: 'SERVICING',
        invoiceno: this.invoiceno,
        sparepart: this.productnamesdatasvalue,
        productname: this.productnamesdatas,
        dateofpurchase: this.dateofpurchases,
        dateofservicerequest: this.dateofservicerequest,
        iswarranty: this.datass,
        charge: this.unitprice,
        techniciancost: this.techniciancost,
        tax: this.tax,
        paidamount: this.paidamount,
        servicingdate: this.servicingdate,
        assignedto: this.employeeNames,
        issuedescription: this.issuedescription,
        ticketstatus: 'PENDING'
      }).subscribe(data => {
        console.log(data)
        if (data['customerids'] > 0) {
          alert('Ticket has been raised for your request with ticketno:-' + data['ticketno'] + ' & ' + 'Resolving Date is' + ' ' + data['dateofresolvingissue'])
          window.location.href = this.ROOT_URL_ANGULAR + '/servicetable'
        } else {
          alert('Something Went Wrong Please Try Again')
        }
      })
    }
  }
  radioChecked(item) {
    this.arraysjs.push(item)
    this.calculateRefund()
  }

  updateRequestData(event) {
    const base_URL = this.ROOT_URL_DEV+'/api/excel/updatereturnproductoffline'
    this.http.post(base_URL, {
      id: this.offlinecustomerid,
      returnedproduct: this.arraysjs,
    }).subscribe(data => {
      alert('Order Details has been sucessfully Updated ')
      window.location.href = this.ROOT_URL_ANGULAR+'/offlinecustomerdetails'
    })
}
  calculateRefund() {
    const base_URL = this.ROOT_URL_DEV + '/api/excel/refundmoneyforofflinecustomer'
    this.http.post(base_URL,{
      returnedproduct: this.arraysjs,
    }).subscribe(data => {
      this.refundmoney = data
      console.log(data)
    })
  }
}