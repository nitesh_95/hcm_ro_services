import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-outwardstockdetails',
  templateUrl: './outwardstockdetails.component.html',
  styleUrls: ['./outwardstockdetails.component.less']
})
export class OutwardstockdetailsComponent implements OnInit {
  purchasedates: string;
  getKeysArrays: string[];
  getValueArrays: any[];
  map: any;
  getdata: string;
  getdatas: string;
  getdatass: string;
  getdatassss: string;
  getValueArrayssss: any[];
  getKeysArraysss: string[];

  constructor(private http: HttpClient, private router: Router) { }

  pageSize = 5
  purchasedate = new Date();
  offlinecustomer: any = []
  onlinecustomer: any = []
  getKeysArray: any
  getValueArray: any
  data:boolean
  datas:boolean
 // ROOT_URL_DEV: string = 'http://localhost:5000'
  //ROOT_URL_ANGULAR:string='http://localhost:4200'
  ROOT_URL_DEV: string ='http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'

  ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'

  ngOnInit() {
    this.getofflinepurchasedetails(event)
    this.getonlinepurchasedetails(event)
  }

  dateofpurchasedetails() {
    this.dateofpurchasedetailsoffline(event)
    this.dateofpurchasedetailsonline(event)
  }

  getofflinepurchasedetails(event) {
    const base_URL = this.ROOT_URL_DEV+'/api/excel/offlineproductcustomerdetails'
    this.http.get(base_URL, {
    }).subscribe(data => {
      this.offlinecustomer.push(data)
      this.offlinecustomer = this.offlinecustomer[0]
    })
  }
  getonlinepurchasedetails(event) {
    const base_URL = this.ROOT_URL_DEV+'/api/excel/onlineproductpurchasedstock'
    this.http.get(base_URL, {
    }).subscribe(data => {
      this.onlinecustomer.push(data)
      this.onlinecustomer = this.onlinecustomer[0]
    })
  }

  getonlineproductdata(event) {
    const base_URL = this.ROOT_URL_DEV+'/api/excel/onlineproductpurchasedstocks/' + this.purchasedates
    this.http.get(base_URL, {
    }).subscribe(data => {
      var myVar = data
      this.getKeysArraysss = Object.keys(myVar);
      this.getValueArrayssss = Object.values(myVar)
      if (this.getKeysArraysss.length == 0) {
        this.getKeysArraysss[0] = "N/A"
      } else if (this.getValueArrayssss.length == 0) {
        this.getValueArrayssss[0] = 0;
      }
     
      this.datas = true
      this.getdatass = this.getKeysArraysss.toString().replace(',', ' ,');
      this.getdatassss = this.getValueArrayssss.toString().replace(',', ' ,');
    })
  }

  getofflineproductdata(event) {
    const base_URL = this.ROOT_URL_DEV+'/api/excel/offlineproductpurchase/' + this.purchasedates
    this.http.get(base_URL, {
    }).subscribe(data => {
      var myVar = data
      this.getKeysArrays = Object.keys(myVar);
      this.getValueArrays = Object.values(myVar)
      if (this.getKeysArrays.length == 0) {
        this.getKeysArrays[0] = 'N/A';
      } else if (this.getValueArrays.length == 0) {
        this.getValueArrays[0] = 0;
      }
      this.data = true
      this.getdata = this.getKeysArrays.toString().replace(',', ',');
      this.getdatas = this.getValueArrays.toString().replace(',', ',');
    })
  }
  dateofpurchasedetailsoffline(event) {
    this.getofflineproductdata(event)
    const base_URL = this.ROOT_URL_DEV+'/api/excel/offlineproductcustomerdetails/' + this.purchasedates
    this.http.get(base_URL, {
    }).subscribe(data => {
      this.offlinecustomer = [];
      this.offlinecustomer.push(data)
      this.offlinecustomer = this.offlinecustomer[0]
    })
  }
  dateofpurchasedetailsonline(event) {
    this.getonlineproductdata(event)
    const base_URL = this.ROOT_URL_DEV+'/api/excel/onlineproductpurchasedstock/' + this.purchasedates
    this.http.get(base_URL, {
    }).subscribe(data => {
      this.onlinecustomer = [];
      this.onlinecustomer.push(data)
      this.onlinecustomer = this.onlinecustomer[0]
    })
  }
}