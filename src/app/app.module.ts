import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { ChartsModule } from 'ng2-charts';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { EmployeedetailsComponent } from './employeedetails/employeedetails.component';
import { HttpClientModule } from '@angular/common/http';
import { ToDoListComponent } from './to-do-list/to-do-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CustomerdetailsComponent } from './customerdetails/customerdetails.component';
import { AddtaskComponent } from './addtask/addtask.component';
import { AddProductComponent } from './add-product/add-product.component';
import { CustomerwebsiteComponent } from './customerwebsite/customerwebsite.component';
import { ShopnowComponent } from './shopnow/shopnow.component';
import { PaymentpageComponent } from './paymentpage/paymentpage.component';
import { PlaceorderComponent } from './placeorder/placeorder.component';
import { ServiceapplyComponent } from './serviceapply/serviceapply.component';
import { GetonlinecustomerlistComponent } from './getonlinecustomerlist/getonlinecustomerlist.component';
import { GetcutomerorderdetailsComponent } from './getcutomerorderdetails/getcutomerorderdetails.component';
import { EmployeetableComponent } from './employeetable/employeetable.component';
import { OfflinecustomerdetailsComponent } from './offlinecustomerdetails/offlinecustomerdetails.component';
import { OfflinecustomerdetailsdataComponent } from './offlinecustomerdetailsdata/offlinecustomerdetailsdata.component';
import { AddofflinecustomerComponent } from './addofflinecustomer/addofflinecustomer.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InvoicegenerationComponent } from './invoicegeneration/invoicegeneration.component';
import { StockdetailsComponent } from './stockdetails/stockdetails.component';
import { StockproducttableComponent } from './stockproducttable/stockproducttable.component';
import { InwardstockComponent } from './inwardstock/inwardstock.component';
import { StocktablelistComponent } from './stocktablelist/stocktablelist.component';
import { OutwardstockdetailsComponent } from './outwardstockdetails/outwardstockdetails.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ServicetableComponent } from './servicetable/servicetable.component';
import { ServicedetailsupdateComponent } from './servicedetailsupdate/servicedetailsupdate.component';
import { ServiceinvoiceComponent } from './serviceinvoice/serviceinvoice.component';
import { TechnicianportalComponent } from './technicianportal/technicianportal.component';
import { TechnicianloginComponent } from './technicianlogin/technicianlogin.component';
import { TechniciandetailsComponent } from './techniciandetails/techniciandetails.component';
import { TechniciantableComponent } from './techniciantable/techniciantable.component';
import { TechnicianTableComponent } from './technician-table/technician-table.component';
import { TechnicianInvoiceComponent } from './technician-invoice/technician-invoice.component';
import { TechniciandashboardComponent } from './techniciandashboard/techniciandashboard.component';
import { ReturnproductpageComponent } from './returnproductpage/returnproductpage.component';
import { ReturnofflinecustomerproductComponent } from './returnofflinecustomerproduct/returnofflinecustomerproduct.component';



@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    LoginComponent,
    EmployeedetailsComponent,
    ToDoListComponent,
    DashboardComponent,
    CustomerdetailsComponent,
    AddtaskComponent,
    AddProductComponent,
    CustomerwebsiteComponent,
    ShopnowComponent,
    PaymentpageComponent,
    PlaceorderComponent,
    ServiceapplyComponent,
    GetonlinecustomerlistComponent,
    GetcutomerorderdetailsComponent,
    EmployeetableComponent,
    OfflinecustomerdetailsComponent,
    OfflinecustomerdetailsdataComponent,
    AddofflinecustomerComponent,
    InvoicegenerationComponent,
    StockdetailsComponent,
    StockproducttableComponent,
    InwardstockComponent,
    StocktablelistComponent,
    OutwardstockdetailsComponent,
    ServicetableComponent,
    ServicedetailsupdateComponent,
    ServiceinvoiceComponent,
    TechnicianportalComponent,
    TechnicianloginComponent,
    TechniciandetailsComponent,
    TechniciantableComponent,
    TechnicianTableComponent,
    TechnicianInvoiceComponent,
    TechniciandashboardComponent,
    ReturnproductpageComponent,
    ReturnofflinecustomerproductComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    Ng2SearchPipeModule,
    ChartsModule,
    Ng2OrderModule,
    NgxPaginationModule,
    HttpClientModule,
    FormsModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }