import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-techniciandashboard',
  templateUrl: './techniciandashboard.component.html',
  styleUrls: ['./techniciandashboard.component.less']
})
export class TechniciandashboardComponent implements OnInit {
  completedtasks: any;
  pendingtasks: any;
  alltasks: any;
  customersatisfaction: any;

  constructor(private http: HttpClient, private router: Router) { }
  name: any
  technicallist: any = []
  // ROOT_URL_DEV: string = 'http://localhost:5000'
  // ROOT_URL_ANGULAR:string='http://localhost:4200'
  ROOT_URL_DEV: string = 'http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'
  ROOT_URL_ANGULAR: string = 'http://delightro.s3-website.ap-south-1.amazonaws.com'
  ngOnInit() {
    this.name = localStorage.getItem('loginname')
    this.getallproductname(event)
  }

  getallproductname(event) {
    const base_URL = this.ROOT_URL_DEV + '/api/excel/technicianportalcount/' +JSON.parse(this.name)
    this.http.get(base_URL,).subscribe(data => {
      this.technicallist.push(data)
      this.technicallist = this.technicallist[0]
      this.completedtasks =  this.technicallist.completedtasks
      console.log(this.completedtasks)
      this.pendingtasks =  this.technicallist.pendingtasks
      this.alltasks =  this.technicallist.alltasks
      this.customersatisfaction =  this.technicallist.customersatisfaction
    })
  }
}
