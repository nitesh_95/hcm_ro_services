import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechniciandashboardComponent } from './techniciandashboard.component';

describe('TechniciandashboardComponent', () => {
  let component: TechniciandashboardComponent;
  let fixture: ComponentFixture<TechniciandashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechniciandashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechniciandashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
