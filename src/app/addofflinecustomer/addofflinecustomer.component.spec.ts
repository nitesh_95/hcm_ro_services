import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddofflinecustomerComponent } from './addofflinecustomer.component';

describe('AddofflinecustomerComponent', () => {
  let component: AddofflinecustomerComponent;
  let fixture: ComponentFixture<AddofflinecustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddofflinecustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddofflinecustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
