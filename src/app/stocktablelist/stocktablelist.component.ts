import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-stocktablelist',
  templateUrl: './stocktablelist.component.html',
  styleUrls: ['./stocktablelist.component.less']
})
export class StocktablelistComponent implements OnInit {
  employeedetailsList: any = [];
  pageSize: 5;
  data: Object;
  arr: any = []
  datas: string;
  map = new Map<String, String>();
  _postsArray: any = [];
  datass: string;
  constructor(private http: HttpClient, private router: Router) { }
   // ROOT_URL_DEV: string = 'http://localhost:5000'
  //ROOT_URL_ANGULAR:string='http://localhost:4200'
  ROOT_URL_DEV: string ='http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'
  ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'
  ngOnInit() {
    this.getdata(event)
    this.getAllCompName(event)
  }
  getAllCompName(event) {
    const base_URL = this.ROOT_URL_DEV+'/api/excel/getallstock'
    this.http.get(base_URL, {
    }).subscribe(data => {
      console.log(data)
      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]
    })
  }


  getdata(event) {
    const base_URL = this.ROOT_URL_DEV+'/api/excel/actualstockdetails'
    this.http.get(base_URL, {
    }).subscribe(data => {
      this.data = data
      console.log(this.data)
    })
  }
  deleteItem(event) {
    alert('Are You Sure You Want to delete the Product')
    var selected_id = event.currentTarget.id
    const base_URL = this.ROOT_URL_DEV+'/api/excel/getallstock/' + selected_id
    this.http.get(base_URL, {
    }).subscribe(data => {
      if (data['status'] == '200' && data['message'] == 'ProductDeleted') {
        alert('product has been deleted')
        window.location.reload()
      } else {
        alert('Something went Wrong')
      }
    })
  }
}