import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StocktablelistComponent } from './stocktablelist.component';

describe('StocktablelistComponent', () => {
  let component: StocktablelistComponent;
  let fixture: ComponentFixture<StocktablelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StocktablelistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocktablelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
