import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { data } from 'jquery';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.less']
})

  export class RegistrationComponent implements OnInit {

 compname:any
 allocated:any
 category:any
 employeedetailsList:any = []
 id:any

  constructor(private router:Router,private http:HttpClient) {
    
  }
   // ROOT_URL_DEV: string = 'http://localhost:5000'
  //ROOT_URL_ANGULAR:string='http://localhost:4200'
  ROOT_URL_DEV: string ='http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'

  ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'
 ngOnInit(){
  this.id = sessionStorage.getItem('username')
this.getAllCompName(event)
 }
 logOut(event){
  sessionStorage.clear()
  this.router.navigate(['login']);

 }
 clickSubmit(event) {
  const base_URL = this.ROOT_URL_DEV+'/save'
  this.http.post(base_URL, {
    compname: this.compname,
    allocated: this.allocated,
    category: this.category,
  }).subscribe(data => {
    console.log(data['status'])
    if (data['status'] == '00') {
      alert("Employee has been Added Successfully")
      window.location.reload();
    }

  })
}
getAllCompName(event){
  const base_URL = this.ROOT_URL_DEV+'/getAll'
  this.http.post(base_URL, {
  }).subscribe(data => {

    this.employeedetailsList.push(data)
    this.employeedetailsList = this.employeedetailsList[0]
    console.log(data)

  })
}
delete(event){
  var selected_id =event.currentTarget.id
  alert("Are you sure you want to delete the Data??")
  const base_URL = this.ROOT_URL_DEV+'/delete/'+ selected_id
  this.http.post(base_URL,{}).subscribe(data =>{
    console.log(data)
    if (data['status'] == '00') {
      alert("Data has been sucessfully deleted")
      window.location.reload();
    }
  })
  }
}