import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicedetailsupdateComponent } from './servicedetailsupdate.component';

describe('ServicedetailsupdateComponent', () => {
  let component: ServicedetailsupdateComponent;
  let fixture: ComponentFixture<ServicedetailsupdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicedetailsupdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicedetailsupdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
