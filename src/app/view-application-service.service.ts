import { Injectable, OnInit } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ViewApplicationServiceService implements OnInit{
  employeeData:any=[]
  employeeDataAugust:any =[]
  employeeDataSeptember:any=[]
  employeeDataOctober:any=[]
  employeeDataNovember:any=[]
  employeeDataDecember:any=[]

  constructor() { }
ngOnInit(){}

employeeAugust(employeeDataAugust:any) {
    this.employeeDataAugust = employeeDataAugust;
 
    sessionStorage.setItem('employeeDataAugust',JSON.stringify(this.employeeDataAugust))
    
  }
  employeeSeptember(employeeDataSeptember:any) {
    this.employeeDataSeptember = employeeDataSeptember;
 
    sessionStorage.setItem('employeeDataSeptember',JSON.stringify(this.employeeDataSeptember))
    
  }
  employeeOctober(employeeDataOctober:any) {
    this.employeeDataOctober = employeeDataOctober;
 
    sessionStorage.setItem('employeeDataOctober',JSON.stringify(this.employeeDataOctober))
    
  }
  employeeNovember(employeeDataNovember:any) {
    this.employeeDataNovember = employeeDataNovember;
 
    sessionStorage.setItem('employeeDataNovember',JSON.stringify(this.employeeDataNovember))
    
  }
  employeeDecember(employeeDataDecember:any) {
    this.employeeDataDecember = employeeDataDecember;
 
    sessionStorage.setItem('employeeDataDecember',JSON.stringify(this.employeeDataDecember))
    
  }
}


