import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-shopnow',
  templateUrl: './shopnow.component.html',
  styleUrls: ['./shopnow.component.less']
})
export class ShopnowComponent implements OnInit {
  employeedetailsList: any = [];
  data: any;
  url: string;
  constructor(private router: Router, private http: HttpClient) { }

  examplearray: any = []
  myarray: any = []
   // ROOT_URL_DEV: string = 'http://localhost:5000'
  //ROOT_URL_ANGULAR:string='http://localhost:4200'
  ROOT_URL_DEV: string ='http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'
  ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'
  ngOnInit() {
    this.getalllist(event);
  }

  getalllist(event) {
    const base_URL = this.ROOT_URL_DEV+'/api/excel/getAllList'
    this.http.get(base_URL, {
    }).subscribe(data => {
      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]
      console.log(data)
    })
  }
  public showCatalogues(event) {
    window.open('./assets/catalogue.pdf', '_blank');
  }

  public showCatalogue(event) {
    window.open('./assets/Livpure.pdf', '_blank');
  }
  priceList(event) {
    if (this.examplearray.length == 0) {
      alert("First add some item to cart")
    } else {
      localStorage.clear()
      this.itemsOnCart(event)
      this.totalamount(event)
      window.open(this.ROOT_URL_ANGULAR+'/paymentpage','_blank')
    }
  }
  totalamount(event) {
    console.log(this.examplearray)
    const base_URL =this.ROOT_URL_DEV +'/api/excel/getTotalAmounts'
    this.http.post(base_URL, this.examplearray).subscribe(r =>
      localStorage.setItem('amount', JSON.stringify(r))

    )
  }

  itemsOnCart(event) {
    console.log(this.examplearray)
    const base_URL = this.ROOT_URL_DEV+'/api/excel/getProductsIdList'
    this.http.post(base_URL, this.examplearray).subscribe(r =>
      localStorage.setItem('Object', JSON.stringify(r))

    )
  }
  removeItem(event) {
    var selected_id = event.currentTarget.id
    const index: number = this.examplearray.indexOf(selected_id);
    this.examplearray.splice(index, 1);
    this.data = this.examplearray.length
  }

  addToCart(event) {
    var selected_id = event.currentTarget.id
    this.myarray = this.examplearray.push(selected_id)
    this.data = this.examplearray.length
    console.log(this.examplearray)
  }
}
function downloadUrl(downloadUrl: any, arg1: string) {
  throw new Error('Function not implemented.');
}

