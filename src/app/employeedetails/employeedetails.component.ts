import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-employeedetails',
  templateUrl: './employeedetails.component.html',
  styleUrls: ['./employeedetails.component.less']
})

export class EmployeedetailsComponent implements OnInit {
  approveCustomerData: any = []
  joineeddates: any

  name: any
  adhar: any
  mobile: any
  address: any
  employeedetailsList: any = [];
  pan: any
  id: any
  updatetargetId: any
  names: any
  adhars: any
  mobiles: any
  addresses: any
  pans: any
  empid: any
  empids: any
  joinedDate: any
  constructor(private router: Router, private http: HttpClient) { }
 // ROOT_URL_DEV: string = 'http://localhost:5000'
  //ROOT_URL_ANGULAR:string='http://localhost:4200'
  ROOT_URL_DEV: string ='http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'

  ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'
  ngOnInit() {
    this.adhar = ''
    this.pan = ''
    this.mobile = ''
    this.empid = ''
    this.name = ''
  }
  logOut(event) {
    sessionStorage.clear()
    this.router.navigate(['login']);

  }
  clickSubmit(event) {
    if (!this.empid || !this.name || !this.adhar || !this.pan || !this.mobile) {
      alert('Every Field is Mandatory')
    } else if (this.adhar.length != 12 || this.pan.length != 10 || this.mobile.length != 10) {
      console.log(this.adhar.length)
      console.log(this.pan.length)
      console.log(this.mobile.length)
      this.adhar = ''
      this.pan = ''
      this.mobile = ''
      alert('Please Provide Correct Details')
    } else {
      const base_URL = this.ROOT_URL_DEV+'/api/excel/employeeentity'
      this.http.post(base_URL, {
        id: this.empid,
        name: this.name,
        adharno: this.adhar,
        pancardno: this.pan,
        mobileno: this.mobile,
        joineddate: this.joinedDate
      }).subscribe(data => {
        if (data['id'] = this.empid) {
          alert("Employee has been Added Successfully")
          window.location.href = this.ROOT_URL_ANGULAR+'/employeetable';
        }
      })
    }
  }

  findbyid(event) {
    var selected_id = event.currentTarget.id
    const base_URL = this.ROOT_URL_DEV+'/api/excel/getemployeebyid/' + selected_id
    this.http.post(base_URL, {
    }).subscribe(data => {
      this.empids = data['id']
      this.names = data['name']
      this.adhars = data['adharno']
      this.pans = data['pancardno']
      this.mobiles = data['mobileno']
      this.joineeddates = data['joineddate']
    })
  }

  delete(event) {
    var selected_id = event.currentTarget.id
    alert("Are You Sure you want to Delete Data ??")
    const base_URL = this.ROOT_URL_DEV+'/delete/' + selected_id
    this.http.post(base_URL, {
    }).subscribe(data => {
      console.log(data['status'])
      if (data['status'] == '00') {
        alert("Employee has been Deleted Successfully")
        window.location.reload();
      }

    })
  }
}