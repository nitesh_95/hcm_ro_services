import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-technician-invoice',
  templateUrl: './technician-invoice.component.html',
  styleUrls: ['./technician-invoice.component.less']
})
export class TechnicianInvoiceComponent implements OnInit {
  technicianinvoice: any;
  customerid: any;
  customertype: any;
  invoiceno: any;
  name: any;
  mailid: any;
  mobileno: any;
  address: any;
  dateofpurchase: any;
  dateofservicerequest: any;
  assignedto: any;
  dateofresolvingissue: any;
  ticketstatus: any;
  charge: any;
  techniciancost: any;
  tax: any;
  totalcost: any;
  paidamount: any;
  remainingamount: any;
  myarray:any  =[]
  ticketno: any;


  constructor() { }

  ngOnInit() {
  
    this.technicianinvoice = JSON.parse(localStorage.getItem('technicianinvoice'))
    console.log(this.technicianinvoice)
    this.customerid = this.technicianinvoice.customerids
    this.customertype = this.technicianinvoice.customertype
    this.invoiceno = this.technicianinvoice.invoiceno
    this.ticketno=this.technicianinvoice.ticketno
    this.name = this.technicianinvoice.name
    this.mailid = this.technicianinvoice.mailid
    this.mobileno = this.technicianinvoice.mobileno
    this.address = this.technicianinvoice.address
    this.dateofpurchase = this.technicianinvoice.dateofpurchase
    this.dateofservicerequest = this.technicianinvoice.dateofservicerequest
    this.assignedto = this.technicianinvoice.assignedto
    this.dateofresolvingissue = this.technicianinvoice.dateofresolvingissue
    this.ticketstatus = this.technicianinvoice.ticketstatus
    this.charge = this.technicianinvoice.charge
    this.techniciancost = this.technicianinvoice.techniciancost
    this.tax = this.technicianinvoice.tax
    this.totalcost = this.technicianinvoice.totalcost
    this.paidamount = this.technicianinvoice.paidamount
    this.remainingamount = this.technicianinvoice.remainingamount
    this.myarray.push(this.technicianinvoice.productPurchaseds)
    this.myarray = this.myarray[0]
    console.log(this.myarray.productnames)
  }
  print(event) {
    window.print()
  }
}
