import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicianInvoiceComponent } from './technician-invoice.component';

describe('TechnicianInvoiceComponent', () => {
  let component: TechnicianInvoiceComponent;
  let fixture: ComponentFixture<TechnicianInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicianInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicianInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
