import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetonlinecustomerlistComponent } from './getonlinecustomerlist.component';

describe('GetonlinecustomerlistComponent', () => {
  let component: GetonlinecustomerlistComponent;
  let fixture: ComponentFixture<GetonlinecustomerlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetonlinecustomerlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetonlinecustomerlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
