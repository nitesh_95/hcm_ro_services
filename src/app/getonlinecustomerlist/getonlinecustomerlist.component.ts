import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-getonlinecustomerlist',
  templateUrl: './getonlinecustomerlist.component.html',
  styleUrls: ['./getonlinecustomerlist.component.less']
})
export class GetonlinecustomerlistComponent implements OnInit {

  public pageSize: number = 5;
  searchText;
  approvedApplications: any = []
  approveCustomerData = [];
  p: number = 1;
  id: any = sessionStorage.getItem('username')
  pendingForReview: any;
  employeedetailsList: any = [];
  approvedApplicationService: any;
  customeriddata: any;
  name: any;
  myarray: any;
  warrantystatus: any;
  transactionid: any;
  paymentdate: any;
  customerid: string;
  is_cancelled: any;
  userObservable: boolean;
  formdata: boolean;
  emailid: any;
  address: any;
  postalcode: any;
  mobileno: any;
  purchaseid: any;
  purchasedate: any;
  modeofpayment: any;
  emiapplicable: any;
  totalamount: any;
  paymentid: any;
  paymentstatus: any;
  dateofdelivery: any;
  orderstatus: any;

  constructor(private router: Router,
    private http: HttpClient) { }
  ngOnInit(): void {
    this.getAllCompName(event);
  }
//  ROOT_URL_DEV: string = 'http://localhost:5000'
//   ROOT_URL_ANGULAR:string='http://localhost:4200'
  ROOT_URL_DEV: string ='http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'

  ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'
  fetchData(event) {
    console.log("data has been recieved")
    this.approveCustomerData = []
    var selected_id = event.currentTarget.id
    this.approvedApplications.forEach(data => {
      if (selected_id == data.id) {
        this.approveCustomerData.push(data)
        this.approvedApplicationService.approvedCustomerDatas(this.approveCustomerData)
        this.router.navigateByUrl('/customerorder')
      }
    })
  }

  getdetails(event) {
    this.getCustomerById(event)
    this.getfeedback(event)
    this.cancellationfeedback(event)
  }

  getfeedback(event) {
    var selected_id = event.currentTarget.id
    const base_URL = this.ROOT_URL_DEV+'/api/excel/getfeedback/' + selected_id
    this.http.get(base_URL, {
    }).subscribe(data => {
      localStorage.setItem('feedback', JSON.stringify(data))
      this.router.navigateByUrl('/customerorder')
      // window.location.href = this.ROOT_URL_ANGULAR+"/customerorder"
    })
  }

  cancellationfeedback(event) {
    var selected_id = event.currentTarget.id
    const base_URL = this.ROOT_URL_DEV+'/api/excel/getfeedbackservice/' + selected_id
    this.http.get(base_URL, {
    }).subscribe(data => {
      localStorage.setItem('cancellationfeedback', JSON.stringify(data))
      this.router.navigateByUrl('/customerorder')
      // window.location.href = this.ROOT_URL_ANGULAR+'/customerorder'
    })
  }
  getCustomerById(event) {
    var selected_id = event.currentTarget.id
    const base_URL = this.ROOT_URL_DEV+'/api/excel/getDetails/' + selected_id
    this.http.get(base_URL, {
    }).subscribe(data => {
      localStorage.setItem('getcustomerbyid', JSON.stringify(data))
      console.log(localStorage.getItem('getcustomerbyid'))
      this.router.navigateByUrl('/customerorder')
      // window.location.href = this.ROOT_URL_ANGULAR+'/customerorder'
    })
  }

  getAllCompName(event) {
    const base_URL = this.ROOT_URL_DEV+'/api/excel/getCustomerOnlineCustomerList'
    this.http.get(base_URL, {
    }).subscribe(data => {
      console.log(data)
      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]
    })
  }
}