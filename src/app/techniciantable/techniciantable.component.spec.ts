import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechniciantableComponent } from './techniciantable.component';

describe('TechniciantableComponent', () => {
  let component: TechniciantableComponent;
  let fixture: ComponentFixture<TechniciantableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechniciantableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechniciantableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
