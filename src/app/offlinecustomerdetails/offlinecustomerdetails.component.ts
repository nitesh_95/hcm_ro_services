import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-offlinecustomerdetails',
  templateUrl: './offlinecustomerdetails.component.html',
  styleUrls: ['./offlinecustomerdetails.component.less']
})
export class OfflinecustomerdetailsComponent implements OnInit {


  employeedetailsList: any = [];
  orderstatusss: any
  catalogsdatas: any = []
   // ROOT_URL_DEV: string = 'http://localhost:5000'
  //ROOT_URL_ANGULAR:string='http://localhost:4200'
  ROOT_URL_DEV: string ='http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'

  ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getAllCompName(event)
  }

  getdetails(event) {
    var selected_id = event.currentTarget.id
    const base_URL = this.ROOT_URL_DEV+'/api/excel/offlinecustomerbyid/' + selected_id
    this.http.get(base_URL, {
    }).subscribe(data => {
      console.log(data)
      localStorage.setItem('offlinecustomerdata', JSON.stringify(data))
      window.location.href =this.ROOT_URL_ANGULAR+"/offlinecustomerdetailsdata"
    })
  }

    getinvoice(event) {
    var selected_id = event.currentTarget.id
    const base_URL = this.ROOT_URL_DEV+'/api/excel/offlinecustomerbyid/' + selected_id
    this.http.get(base_URL, {
    }).subscribe(data => {
      console.log(data)
      localStorage.setItem('offlinecustomerdata', JSON.stringify(data))
      window.location.href =this.ROOT_URL_ANGULAR+'/invoicegeneration'
    })
  }
  getAllCompName(event) {
    const base_URL = this.ROOT_URL_DEV+'/api/excel/getofflinecustomerdetails'
    this.http.get(base_URL, {
    }).subscribe(data => {
      console.log(data)
      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]
    })
  }
}
