import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicegenerationComponent } from './invoicegeneration.component';

describe('InvoicegenerationComponent', () => {
  let component: InvoicegenerationComponent;
  let fixture: ComponentFixture<InvoicegenerationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicegenerationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicegenerationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
