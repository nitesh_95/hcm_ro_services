import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-stockdetails',
  templateUrl: './stockdetails.component.html',
  styleUrls: ['./stockdetails.component.less']
})
export class StockdetailsComponent implements OnInit {

  itemname: any
  itemcode: any
  itemdescription: any
  itemcategory: any
  hsncode: any
  purchasedfrom: any
  isButtonEnabled: boolean = false
  itemlocation: any;
  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit() {
  }
   // ROOT_URL_DEV: string = 'http://localhost:5000'
  //ROOT_URL_ANGULAR:string='http://localhost:4200'
  ROOT_URL_DEV: string ='http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'
  ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'

  submitProduct(event) {
    if (!this.itemname || !this.itemcode || !this.itemdescription || !this.hsncode || !this.itemlocation) {
      alert('Please fill the details before proceeding')
    } else {
      this.isButtonEnabled = false
      const base_URL = this.ROOT_URL_DEV+'/api/excel/saveStockProduct'
      this.http.post(base_URL, {
        itemcategory: this.itemcategory,
        itemname: this.itemname,
        description: this.itemdescription,
        itemcode: this.itemcode,
        hsncode: this.hsncode,
        itemlocation:this.itemlocation
      }).subscribe(data => {
        if (data['id'] >= 1) {
          this.isButtonEnabled = true
          window.location.reload()
          window.location.href=this.ROOT_URL_ANGULAR+'/inwardstock'
          alert('ProductAddedSucessfully')
        } else {
          alert('Something went Wrong')
        }
      }
      )
    }
  }
}

