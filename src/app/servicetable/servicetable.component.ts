import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-servicetable',
  templateUrl: './servicetable.component.html',
  styleUrls: ['./servicetable.component.less']
})
export class ServicetableComponent implements OnInit {
  categories: any = [];
  selected_id: any;
  selected_ids: any;
  ticketstatus: any;
  ticketname: any;
  customername: any;
  ticketno: any;
  customertype: any;
  mobileno: any;
  version: any;
  type: any;
  sparepart: any;
  productname: any;
  dateofpurchase: any;
  dateofservicerequest: any;
  iswarranty: any;
  charge: any;
  techniciancost: any;
  tax: any;
  totalcost: any;
  paidamount: any;
  dateofresolving: any;
  servicingdate: any;
  remainingamount: any;
  invoiceno: any;
  customerids: any;
  name: any;
  mailid: any;
  address: any;
  model: any;
  dateofservicing: any
  customerId: any

  dateofresolvingissue: any;
  paidservicingamount: any;
   // ROOT_URL_DEV: string = 'http://localhost:5000'
  //ROOT_URL_ANGULAR:string='http://localhost:4200'
  ROOT_URL_DEV: string ='http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'

  ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'
  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.getAllCompName(event)
  }


  invoiceservice(event) {
    this.selected_id = event.currentTarget.id
    const base_URL = this.ROOT_URL_DEV+'/api/excel/servicingdetailslist/' + this.selected_id
    this.http.get(base_URL, {
    }).subscribe(data => {
      localStorage.setItem('categories', JSON.stringify(data))
      window.location.href = this.ROOT_URL_ANGULAR+'/serviceinvoice'
      this.customerId = data['customerids']
      this.customertype = data['customertype']
      this.name = data['name']
      this.mailid = data['mailid']
      this.address = data['address']
      this.mobileno = data['mobileno']
      this.model = data['model']
      this.type = data['type']
      this.sparepart = data['sparepart']
      this.productname = data['productname']
      this.dateofpurchase = data['dateofpurchase']
      this.dateofservicerequest = data['dateofservicerequest']
      this.iswarranty = data['iswarranty']
      this.charge = data['charge']
      this.techniciancost = data['techniciancost']
      this.tax = data['tax']
      this.totalcost = data['totalcost']
      this.paidamount = data['paidamount']
      this.ticketno = data['ticketno']
      this.dateofresolvingissue = data['dateofresolvingissue']
      this.servicingdate = data['servicingdate']
      this.remainingamount = data['remainingamount']
      this.ticketstatus = data['ticketstatus']
      this.invoiceno = data['invoiceno']
      console.log(data)

    })
  }


  getAllCompName(event) {
    const base_URL = this.ROOT_URL_DEV+'/api/excel/servicingdetailslist'
    this.http.get(base_URL, {
    }).subscribe(data => {
      console.log(data)
      this.categories.push(data)
      this.categories = this.categories[0]
    })
  }
  UpdateServicingDetails(event) {
    this.selected_id = event.currentTarget.id
    const base_URL = this.ROOT_URL_DEV+'/api/excel/servicingdetails'
    this.http.post(base_URL, {
      customerids: this.customerId,
      customertype: this.customertype,
      name: this.customername,
      mailid: this.mailid,
      address: this.address,
      mobileno: this.mobileno,
      model: this.version,
      type: this.type,
      sparepart: this.sparepart,
      productname: this.productname,
      dateofpurchase: this.dateofpurchase,
      dateofservicerequest: this.dateofservicerequest,
      iswarranty: this.iswarranty,
      charge: this.charge,
      techniciancost: this.techniciancost,
      tax: this.tax,
      totalcost: this.totalcost,
      paidamount: this.paidamount,
      ticketno: this.ticketno,
      dateofresolvingissue: this.dateofresolving,
      servicingdate: this.servicingdate,
      remainingamount: this.remainingamount,
      ticketstatus: this.ticketstatus,
      invoiceno: this.invoiceno,
      paidservicingamount: this.paidservicingamount
    }).subscribe(data => {
      console.log(data)
      if (data['customerids'] > this.selected_id) {
        window.location.href = this.ROOT_URL_ANGULAR+'/servicetable'
      }
    })
  }

  saveServicingDetails(event) {
    this.selected_id = event.currentTarget.id
    const base_URL = this.ROOT_URL_DEV+'/api/excel/servicingdetailslist/' + this.selected_id
    this.http.get(base_URL, {
    }).subscribe(data => {
      this.customerId = data['customerids']
      this.customertype = data['customertype']
      this.name = data['name']
      this.mailid = data['mailid']
      this.address = data['address']
      this.mobileno = data['mobileno']
      this.model = data['model']
      this.type = data['type']
      this.sparepart = data['sparepart']
      this.productname = data['productname']
      this.dateofpurchase = data['dateofpurchase']
      this.dateofservicerequest = data['dateofservicerequest']
      this.iswarranty = data['iswarranty']
      this.charge = data['charge']
      this.techniciancost = data['techniciancost']
      this.tax = data['tax']
      this.totalcost = data['totalcost']
      this.paidamount = data['paidamount']
      this.ticketno = data['ticketno']
      this.dateofresolvingissue = data['dateofresolvingissue']
      this.servicingdate = data['servicingdate']
      this.remainingamount = data['remainingamount']
      this.ticketstatus = data['ticketstatus']
      this.invoiceno = data['invoiceno']
      console.log(data)

    })
  }

}
