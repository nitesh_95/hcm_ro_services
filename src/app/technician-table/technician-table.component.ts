import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-technician-table',
  templateUrl: './technician-table.component.html',
  styleUrls: ['./technician-table.component.less']
})
export class TechnicianTableComponent implements OnInit {

  data: any = []

  constructor() { }

  ngOnInit() {
    var items = localStorage.getItem('servicingEntities')
    this.data.push(JSON.parse(items))
    this.data = this.data[0]
    console.log(this.data)
  }
}
