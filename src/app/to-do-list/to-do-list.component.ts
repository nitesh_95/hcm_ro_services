import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.less']
})
export class ToDoListComponent implements OnInit {
  empid: any
  date: any
  taskdescription: any
  completeddate: any
  tenure: any
  todotask: any
  employeedetailslist: any = []
  taskdates: any;
  taskdescriptions: any;
  statuss: any;
  pans: any;
  dates: any;
  tenures: any;
  userIds: any;
  taskno: any
  employeepanel: any[] = []
  tasknos: any;
  empids: any;
  employee: any[];
  empidsdata: any;
  selectedDay: any;
  data: any;
  constructor(private http: HttpClient, private router: Router) { }
   // ROOT_URL_DEV: string = 'http://localhost:5000'
  //ROOT_URL_ANGULAR:string='http://localhost:4200'
  ROOT_URL_DEV: string ='http://delightropoint-env.eba-efg7cyyp.ap-south-1.elasticbeanstalk.com'
  ROOT_URL_ANGULAR:string='http://delightro.s3-website.ap-south-1.amazonaws.com'
  ngOnInit() {
    this.getalllist(event)
  }

  logOut(event) {
    sessionStorage.clear()
    alert("Are you sure you want to Exit ?")
    this.router.navigate(['login']);

  }
  selectChangeHandler(event: any) {
    this.selectedDay = event.target.value;
    console.log(this.selectedDay)
  }

  todoList(event) {
    this.selectChangeHandler
    const base_URL = this.ROOT_URL_DEV+'/createTask'
    this.http.post(base_URL, {
      createdby: "admin",
      taskdescription: this.taskdescription,
      taskdate: this.date,
      status: this.selectedDay,
      tenure: this.tenure,
      employee: {
        userId: this.empid
      }
    }).subscribe(data => {
      if (data['status'] == '200' && data['message'] == 'Data has been saved') {
        alert('Task added Successfully')
        window.location.reload()
      } else {
        alert('Employee is not present')
      }

    })

  }
  updated(event) {
    var selected_id = event.currentTarget.id
    const base_URL = this.ROOT_URL_DEV+'/employeeGetTaskById/' + selected_id
    this.http.get(base_URL, {
    }).subscribe(data => {
      this.empidsdata = data['employee']
      this.empids = this.empidsdata.userId
      this.tasknos = data['taskno']
      this.taskdates = data['taskdate']
      this.taskdescriptions = data['taskdescription']
      this.tenures = data['tenure']
      this.dates = data['date']
      this.statuss = data['status']
    })
  }

  checkemptyfield() {
    if (this.tenure = '') {
      this.data = this.tenures
    } else {
      this.data = this.tenure
    }
  }


  update(event) {

    if (this.tenure = '') {
      alert("Mandatory fields cant be empty")
    } else {
      var selected_id = event.currentTarget.id
      console.log(selected_id)
      const base_URL = this.ROOT_URL_DEV+'/editTask'
      this.http.post(base_URL, {
        taskno: selected_id,
        createdby: "admin",
        taskdescription: this.taskdescriptions,
        taskdate: this.date,
        status: this.selectedDay,
        tenure: this.tenure,
        employee: {
          userId: this.empids
        }

      }).subscribe(data => {
        if (data['status'] == '200' && data['message'] == 'Data has been saved') {
          alert('Task Updated Successfully')
          window.location.reload()
        } else {
          alert('Employee is not present')
        }
      })
    }
  }
  getalllist(event) {
    const base_URL = this.ROOT_URL_DEV+'/getAllTask'
    this.http.get(base_URL, {
    }).subscribe(data => {
      this.employeedetailslist.push(data)
      this.employeedetailslist = this.employeedetailslist[0]
      console.log(data)
    })
  }


  delete(event) {
    var tasknoData = event.currentTarget.id
    const base_URL = this.ROOT_URL_DEV+'/deleteTask/' + tasknoData
    this.http.post(base_URL, {}).subscribe(data => {
      if (data['status'] == '200') {
        alert("Data has been sucessfully deleted")
        window.location.reload();
      }
    })
  }
}